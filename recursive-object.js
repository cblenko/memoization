function expensive(recur, n) {
    return n * recur(n - 1);
}

function memoizer(memo, fn) {
    var recur = function(n) {
        var result;

        if(memo.hasOwnProperty(n)) {
            result = memo[n];
        } else {
            result = fn(recur, n);
            memo[n] = result;
        }
       
        return result;
    }
    
    return recur;
}

var facUsingObject = memoizer({ "0" : 1, "1" : 1, "2" : 2, "3": 6 }, expensive)


console.log(facUsingObject(10));
console.log(facUsingObject(10));
console.log(facUsingObject(9));
console.log(facUsingObject(15));
