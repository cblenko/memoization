function expensive(v) {
    return v * v * v;
}

function memoizer(fn) {
    var memo = {};

    return function(n) {
        if(memo.hasOwnProperty(n))
            return memo[n];

        var result = fn(n);
        memo[n] = result;
        return result;
    }
}

var memoExpensive = memoizer(expensive);

console.log(memoExpensive(5));
console.log(memoExpensive(5));
console.log(memoExpensive(6));
console.log(memoExpensive(5));
console.log(memoExpensive(6));
console.log(memoExpensive(5));
console.log(memoExpensive(5));

