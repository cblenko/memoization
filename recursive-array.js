function memoizer(memo, fn) {
    var recur = function(n) {
        var result = memo[n];
        if(typeof result !== 'number') {
            result = fn(recur, n);
            memo[n] = result;
        }
        return result;
    }
    
    return recur;
}
var facUsingArray = memoizer([1, 1, 2, 6], function (recur, n) {
    return n * recur(n - 1);
})